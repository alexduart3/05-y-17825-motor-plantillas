const {query} = require("express");
const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database(
    "./db/integrantes.sqlite",
    sqlite3.OPEN_READWRITE,
    (error) => {
        if (error) {
            console.error('Error al conectar con la base de datos:', error.message);
        } else {
            console.log('Conexión exitosa con la base de datos.');
            db.run("select * from integrantes")
        }
    }
);

async function getAll(query, params) {
    return new Promise((resolve, reject) => {
        db.all(query, params, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

async function run(query, params) {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (error) {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        })
    })
}

async function getLastOrder(tablaNombre) {
    const result = await getAll(`SELECT MAX(orden) as maxOrder FROM ${tablaNombre}`);
    return result[0].maxOrder;
}

async function matriculaExistente(matricula) {
    const result = await getAll("SELECT 1 FROM integrantes WHERE matricula = ?", [matricula]);
    return result.length > 0;
}

async function getLastId(tablaNombre) {
    const result = await getAll(`SELECT MAX(id) as maxId FROM ${tablaNombre}`);
    return result[0].maxId;
}

async function matriculaEnMedia(matricula) {
    const result = await getAll("SELECT 1 FROM media WHERE matricula = ? AND activo = 1", [matricula]);
    return result.length > 0;
}

async function tipoMediaEnMedia(id) {
    const result = await getAll("SELECT 1 FROM media WHERE tipoMedia = ? AND activo = 1", [id]);
    return result.length > 0;
}

async function get(query, params) {
    return new Promise((resolve, reject) => {
        db.get(query, params, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

module.exports = {
    db,
    getAll,
    run,
    getLastOrder,
    matriculaExistente,
    getLastId,
    matriculaEnMedia,
    tipoMediaEnMedia,
    get
};

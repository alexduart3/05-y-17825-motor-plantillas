const {getAll} = require("../../db/conexion");

const PaginasController = {
    index: async (req, res) => {
        const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
        const home = await getAll("select * from home");
        res.render("index", {
            home: home[0],
            integrantes: primerNombreIntegrantes,
            isHome: true
        });
    },

    indexWordCloud: async (req, res) => {
        const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
        res.render("word_cloud", {
            integrantes: primerNombreIntegrantes
        });
    },

    indexCurso: async (req, res) => {
        const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
        res.render("curso", {
            integrantes: primerNombreIntegrantes
        });
    },

    indexIntegranteMatricula: async (req, res, next) => {
        const matriculas = (await getAll("select matricula from integrantes where activo = 1 order by orden")).map(obj => obj.matricula);
        const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by orden");
        const matricula = req.params.matricula;
        const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);

        if (matriculas.includes(matricula)) {
            const integranteFilter = await getAll("select * from integrantes where activo = 1 and matricula = ? order by orden", [matricula]);
            const mediaFilter = await getAll("select * from media where activo = 1 and matricula = ? order by orden", [matricula]);
            res.render('integrante', {
                integrante: integranteFilter,
                tipoMedia: tipoMedia,
                media: mediaFilter,
                integrantes: primerNombreIntegrantes
            });
        } else {
            next();
        }
    }
}

// Función para mostrar solo el primer nombre de los integrantes
function obtenerPrimerNombre(integrantes) {
    return integrantes.map(integrante => {
        const primerNombre = integrante.nombre.split(" ")[0];
        return {...integrante, nombre: primerNombre};
    })
}

module.exports = PaginasController;

const {getAll, getLastOrder, matriculaExistente, run, matriculaEnMedia, get} = require("../../db/conexion");

/*index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro*/

const IntegrantesController = {
    index: async function (req, res) {
        const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
        res.render("admin/integrantes/index", {
            integrantes: integrantes
        });
    },

    create: function (req, res) {
        res.render("admin/integrantes/crearIntegrante", {
            matricula: req.query.matricula,
            nombre: req.query.nombre,
            apellido: req.query.apellido
        });
    },

    store: async function (req, res) {
        let errores = [];

        const lastOrder = await getLastOrder('integrantes');
        const newOrder = lastOrder + 1;

        if (req.body.matricula === '') {
            errores.push('¡La matrícula no puede estar vacía!');
        }

        if (await matriculaExistente(req.body.matricula)) {
            errores.push('¡La matrícula ya existe!');
        }

        if (req.body.nombre === '' || req.body.nombre.length > 50) {
            errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.apellido === '' || req.body.apellido.length > 50) {
            errores.push('¡El apellido no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.activo === undefined) {
            errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent(errores.join(';'))}
            &matricula=${encodeURIComponent(req.body.matricula)}
            &nombre=${encodeURIComponent(req.body.nombre)}
            &apellido=${encodeURIComponent(req.body.apellido)}`);
        } else {
            try {
                await run("insert into integrantes (orden, matricula, nombre, apellido, activo) values (?, ?, ?, ?, ?)",
                    [
                        newOrder,
                        req.body.matricula,
                        req.body.nombre,
                        req.body.apellido,
                        req.body.activo
                    ]);
                res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },

    update: async function (req, res) {
        let errores = [];

        const matricula = req.params.matricula;

        if (req.body.nombre === '' || req.body.nombre.length > 50) {
            errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.apellido === '' || req.body.apellido.length > 50) {
            errores.push('¡El apellido no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/integrantes/edit/${matricula}?error=${encodeURIComponent(errores.join(';'))}`);
        } else {
            try {
                await run("UPDATE integrantes SET nombre = ?, apellido = ? WHERE matricula = ?", [req.body.nombre, req.body.apellido, matricula]);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Integrante actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al actualizar al integrante!'));
            }
        }
    },

    edit: async function (req, res) {
        const matricula = req.params.matricula;
        try {
            const integrante = await get("SELECT * FROM integrantes WHERE matricula = ?", [matricula]);
            res.render("admin/integrantes/editarIntegrante", {
                integrante: integrante
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al obtener al integrante!'));
        }
    },

    destroy: async function (req, res) {
        const matricula = req.params.matricula;
        if (await matriculaEnMedia(matricula)) {
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡No se puede eliminar el integrante porque tiene media asociada!'));
        } else {
            try {
                await run("UPDATE integrantes SET activo = 0 WHERE matricula = ?", [matricula]);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    },

    /*Serán métodos implementados en el futuro*/
    /*show: function () {},*/
}

module.exports = IntegrantesController;

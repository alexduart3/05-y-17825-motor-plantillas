const {getAll, getLastOrder, getLastId, run, tipoMediaEnMedia, get} = require("../../db/conexion");

const tipoMediaController = {
    index: async function (req, res) {
        const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by orden");
        res.render("admin/tipoMedia/index", {
            tipoMedia: tipoMedia
        });
    },

    create: function (req, res) {
        res.render("admin/tipoMedia/crearTipoMedia", {
            nombre: req.query.nombre,
        });
    },

    store: async function (req, res) {
        let errores = [];

        const lastOrder = await getLastOrder('tipoMedia');
        const newOrder = lastOrder + 1;
        const lastId = await getLastId('tipoMedia');
        const newId = lastId + 1;

        if (req.body.nombre === '' || req.body.nombre.length > 50) {
            errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.activo === undefined) {
            errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/tipoMedia/crear?error=${encodeURIComponent(errores.join(';'))}&nombre=${encodeURIComponent(req.body.nombre)}`);
        } else {
            try {
                await run("insert into tipoMedia (id, orden, nombre, activo) values (?, ?, ?, ?)",
                    [
                        newId,
                        newOrder,
                        req.body.nombre,
                        req.body.activo
                    ]);
                res.redirect(`/admin/tipoMedia/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/tipoMedia/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },

    update: async function (req, res) {
        let errores = [];

        const id = parseInt(req.params.id);

        if (req.body.nombre === '' || req.body.nombre.length > 50) {
            errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/tipoMedia/edit/${id}?error=${encodeURIComponent(errores.join(';'))}`);
        } else {
            try {
                await run("UPDATE tipoMedia SET nombre = ? WHERE id = ?", [req.body.nombre, id]);
                res.redirect("/admin/tipoMedia/listar?success=" + encodeURIComponent('¡Tipo media actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al actualizar el tipo media!'));
            }
        }
    },

    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            const tipoMedia = await get("SELECT * FROM tipoMedia WHERE id = ?", [id]);
            res.render("admin/tipoMedia/editarTipoMedia", {
                tipoMedia: tipoMedia
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al obtener el tipo media!'));
        }
    },

    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        if (await tipoMediaEnMedia(id)) {
            res.redirect(`/admin/tipoMedia/listar?error=${encodeURIComponent('¡No se puede eliminar el registro porque está siendo utilizado en la tabla media!')}`);
        } else {
            try {
                await run("UPDATE tipoMedia SET activo = 0 WHERE id = ?", [id]);
                res.redirect("/admin/tipoMedia/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    },

    /*Serán métodos implementados en el futuro*/
    /*show: function () {},*/
}

module.exports = tipoMediaController;

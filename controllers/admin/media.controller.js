const {getAll, getLastOrder, getLastId, run, get} = require("../../db/conexion");
const fs = require('fs').promises;

const mediaController = {
    index: async function (req, res) {
        const media = await getAll(`
        SELECT media.*, tipoMedia.nombre as tipoMediaNombre, integrantes.nombre as integranteNombre, integrantes.apellido as integranteApellido
        FROM media
        LEFT JOIN tipoMedia ON media.tipoMedia = tipoMedia.id
        LEFT JOIN integrantes ON media.matricula = integrantes.matricula
        WHERE media.activo = 1
        ORDER BY media.orden
        `);
        res.render("admin/media/index", {
            media: media,
        });
    },

    create: async function (req, res) {
        const integrantes = await getAll("select * from integrantes where activo = 1 order by nombre");
        const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by nombre");
        res.render("admin/media/crearMedia", {
            integrantes: integrantes,
            tipoMedia: tipoMedia,
            tipoMediaSeleccionado: req.query.tipoMedia,
            url: req.query.url,
            titulo: req.query.titulo,
            alt: req.query.alt,
            integranteSeleccionado: req.query.integrante,
        });
    },

    store: async function (req, res) {
        let errores = [];

        const lastOrder = await getLastOrder('media');
        const newOrder = lastOrder + 1;
        const lastId = await getLastId('media');
        const newId = lastId + 1;

        if (req.body.tipoMedia === '' || req.body.tipoMedia === undefined) {
            errores.push('¡Debe seleccionar un tipo de media!');
        }

        if (req.body.integrante === '' || req.body.integrante === undefined) {
            errores.push('¡Debe seleccionar un integrante!');
        }

        if (req.body.url && req.file) {
            errores.push('¡No puedes agregar tanto URL como SRC al mismo tiempo!');
        }

        if (req.body.titulo === '' || req.body.titulo.length > 50) {
            errores.push('¡El título no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.alt === '' || req.body.titulo.length > 80) {
            errores.push('¡El alt no puede estar vacío y debe tener una longitud máxima de 80 caracteres!');
        }

        if (req.body.activo === undefined) {
            errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/media/crear?error=${encodeURIComponent(errores.join(';'))}
            &tipoMedia=${encodeURIComponent(req.body.tipoMedia)}
            &url=${encodeURIComponent(req.body.url)}
            &titulo=${encodeURIComponent(req.body.titulo)}
            &alt=${encodeURIComponent(req.body.alt)}
            &integrante=${encodeURIComponent(req.body.integrante)}`);
        } else {
            let srcPath = '';
            if (req.file) {
                var tpm_path = req.file.path;
                var destino = "public/assets/images/" + req.file.originalname;
                try {
                    await fs.rename(tpm_path, destino);
                    srcPath = "/assets/images/" + req.file.originalname;
                } catch (err) {
                    return res.sendStatus(500);
                }
            }
            try {
                await run("insert into media (id, url, src, titulo, alt, tipoMedia, matricula, orden, activo) values (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    [
                        newId,
                        req.body.url,
                        srcPath,
                        req.body.titulo,
                        req.body.alt,
                        req.body.tipoMedia,
                        req.body.integrante,
                        newOrder,
                        req.body.activo,
                    ]);
                res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/media/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },

    update: async function (req, res) {
        let errores = [];

        const id = parseInt(req.params.id);

        if (req.body.tipoMedia === '' || req.body.tipoMedia === undefined) {
            errores.push('¡Debe seleccionar un tipo de media!');
        }

        if (req.body.integrante === '' || req.body.integrante === undefined) {
            errores.push('¡Debe seleccionar un integrante!');
        }

        if (req.body.url && req.file) {
            errores.push('¡No puedes agregar tanto URL como SRC al mismo tiempo!');
        }

        if (req.body.titulo === '' || req.body.titulo.length > 50) {
            errores.push('¡El título no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }

        if (req.body.alt === '' || req.body.titulo.length > 80) {
            errores.push('¡El alt no puede estar vacío y debe tener una longitud máxima de 80 caracteres!');
        }

        const existingMedia = await get("SELECT * FROM media WHERE id = ?", [id]);
        if (existingMedia.src && req.body.url) {
            errores.push('¡No puedes agregar una URL porque ya existe un SRC!');
        }

        if (existingMedia.url && req.file) {
            errores.push('¡No puedes agregar un SRC porque ya existe una URL!');
        }

        if (errores.length > 0) {
            res.redirect(`/admin/media/edit/${id}?error=${encodeURIComponent(errores.join(';'))}`);
        } else {
            let srcPath = '';
            if (existingMedia.src) {
                srcPath = existingMedia.src;
            }
            if (req.file) {
                var tpm_path = req.file.path;
                var destino = "public/assets/images/" + req.file.originalname;
                try {
                    await fs.rename(tpm_path, destino);
                    srcPath = "/assets/images/" + req.file.originalname;
                } catch (err) {
                    return res.sendStatus(500);
                }
            }
            try {
                await run("UPDATE media SET url = ?, src = ?, titulo = ?, alt = ?, tipoMedia = ?, matricula = ? WHERE id = ?",
                    [
                        req.body.url,
                        srcPath,
                        req.body.titulo,
                        req.body.alt,
                        req.body.tipoMedia,
                        req.body.integrante,
                        id
                    ]);
                res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Media actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/media/edit/" + id + "?error=" + encodeURIComponent('¡Error al actualizar el registro!'));
            }
        }
    },

    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        const integrantes = await getAll("select * from integrantes where activo = 1 order by nombre");
        const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by nombre");
        try {
            const media = await get("SELECT * FROM media WHERE id = ?", [id]);
            res.render("admin/media/editarMedia", {
                integrantes: integrantes,
                tipoMedia: tipoMedia,
                media: media
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al obtener el registro!'));
        }
    },

    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            await run("UPDATE media SET activo = 0 WHERE id = ?", [id]);
            res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
        } catch (err) {
            console.log(err);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
        }
    },

    /*Serán métodos implementados en el futuro*/
    /*show: function () {},*/
}

module.exports = mediaController;
